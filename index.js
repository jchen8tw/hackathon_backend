const express = require("express");
const app = express();
const fetch = require('node-fetch');
global.Headers = fetch.Headers;
var http = require("http").createServer(app);
const port = process.env.PORT || 7122;
var io = require("socket.io")(http);
const data = require('./db.js');
const text = (message) => {
  return { messages:[{type:"text",text:message}]};
}
const send_expire_message = async (obj_name) =>{
    let res = await fetch('https://api.line.me/v2/bot/message/multicast',
    {
    method:'POST',
    body:JSON.stringify({"to" : ["Uc6be4ba2c74c40529626c82ac44502d9"],"messages": [{"type" : "text","text" : `尊敬的客戶您好：${obj_name} 再一天就要過期了!\n請及早處理喔～`}], "notificationDisabled" : false }),
    headers: new Headers(
            { 
            'Content-Type': 'application/json',
            'Authorization':"Bearer jK290/4pZkof5Va6omWR9Sm+l3f1IHlDl75SqSGTLgcF8IJMVwTEScy8G3mIAWXsny3byDIM2UX8Otyu+m/ISwXO5WskW4cwLQp6D37QJEmbWqfJdWkM7OFOx/9WfQBhl6zA+20aN7l3tL3xWiFnxQdB04t89/1O/w1cDnyilFU="
            })});
    res = await res.json();
    return;
}
app.get("/item_list",async (req,res) =>{
    message = "你現在有的東西是：\n";
    for(item of Object.keys(data)){
      message = message + item + "\n過期日：" + data[item].expiration_date.toDateString() +"\n"
    }
    return res.send(text(message));
});
io.on("connection", function(socket) {
    console.log("a user connected");
    socket.on("get_items",()=>{
        socket.emit("item_list",data);
        console.log(data);
    });
    /*
    socket.on("insert_item", item=>{
        data = {...data,item};
        socket.emit("item_list",data);
        console.log(data);
    });
    
    socket.on("remove_item", item=>{
        delete data[item]; 
        console.log(data);   
    });
    */
    socket.on('orange', () =>{
        if( !data['orange']){
            data['orange'] = {expiration_date: new Date((new Date()).valueOf() + 86400000*2)}
        }
        else{
            delete data['orange'];
        }
        socket.emit("item_list",data);
        console.log('orange');
    });
    socket.on('keyboard',()=>{
        if( !data['keyboard']){
            data['keyboard'] = {expiration_date: new Date((new Date()).valueOf() + 86400000*3)}
        }
        else{
            delete data['keyboard'];
        }
        socket.emit("item_list",data);
       console.log('keyboard');     
    });
    socket.on('umbrella', ()=>{
        if( !data['umbrella']){
            data['umbrella'] = {expiration_date: new Date((new Date()).valueOf() + 86400000*0.5)}
        }
        else{
            delete data['umbrella'];
        }
        socket.emit("item_list",data);
        send_expire_message('umbrella');
        console.log('umbrella');
    });
    socket.on('book',()=>{
        if( !data['book']){
            data['book'] = {expiration_date: new Date((new Date()).valueOf() + 86400000*4)}
        }
        else{
            delete data['book'];
        }
        socket.emit("item_list",data);
        console.log('book');
    });
});
/*
data: [
    { 
    name : str,
    expiration_data: date
    }
*/
http.listen(port, function() {
  console.log(`listening on *:${port}`);
});    
    
